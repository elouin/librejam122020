extends Button

func _on_button_up() -> void:
    PlayerData.player_disintegrations = 0
    PlayerData.player_score = 0
    get_tree().paused = false
    get_tree().reload_current_scene()
