tool
extends Button
export (PackedScene) var NextScene

func _on_button_up() -> void:
    get_tree().change_scene_to(NextScene)
