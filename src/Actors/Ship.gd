extends RigidBody2D

export (int) var spin_power = 500
export (int) var engine_power = 220
export (int) var max_gravity = 200
export (int) var gravity_force = 1e6
export (int) var max_lin_velo = 150
export (int) var max_ang_velo = 10
export (float) var steering_break_value = 0.1
export (int) var max_fuel = 10
var astral_bodies = Array()
var thrust = Vector2()
var gravity = Vector2()
var steering_direction = 0
var state = null
var rel_pos = Vector2()
var gravity_pull
var fuel setget set_fuel
var start_body
var target_body
enum {INACTIVE, ACTIVE, DISINTEGRATED}

signal fuel_changed
signal disintegrated
signal target_reached

func _ready() -> void:
    $ThrusterFlame.hide()
    change_state(INACTIVE)

func _process(delta) -> void:
    get_input(delta)
    get_gravity()

func _integrate_forces(physics_state) -> void:
    # apply forces from thrusters and gravity
    set_applied_force(thrust.rotated(rotation) + gravity)
    # limit speed for better experience
    if get_linear_velocity().length() > max_lin_velo:
        set_linear_velocity(get_linear_velocity().normalized() * max_lin_velo)
    # apply torque for steering
    if steering_direction != 0:
        set_applied_torque(spin_power * steering_direction)
    # slow rotation if there is no steering input
    else:
        if abs(get_angular_velocity()) > steering_break_value:
            set_applied_torque(- spin_power * sign(get_angular_velocity()))
        elif abs(get_angular_velocity()) > 0:
            set_applied_torque(- spin_power * get_angular_velocity() / steering_break_value)
            
func get_gravity() -> void:
    gravity = Vector2(0,0)
    # iterate over planets and apply forces depending on planet size and distance
    for astral_body_i in astral_bodies:
        rel_pos = (astral_body_i.get_position() - get_position())
        gravity_pull = gravity_force / pow(rel_pos.length(), 3)
        gravity += astral_body_i.get_astral_body_mass() * gravity_pull * rel_pos.normalized()
    # limit gravity forces, to prevent ship from sticking to planets
    if gravity.length() > max_gravity:
        gravity = gravity.normalized() * max_gravity

func change_state(new_state) -> void:
    match new_state:
        # collision off, no input, sprite alpha = 60% 
        INACTIVE:
            $CollisionShape2D.set_deferred("disabled", false)
            $Sprite.modulate.a = 0.8
        # collision on, input possible, sprite alpha = 100%
        ACTIVE:
            $CollisionShape2D.set_deferred("disabled", false)
            $Sprite.modulate.a = 1.0
        # collision off, no input, hide sprite, emit signal
        DISINTEGRATED:
            $CollisionShape2D.set_deferred("disabled", true)
            $Sprite.modulate.a = 0.0
    state = new_state

func activate():
    change_state(ACTIVE)

func set_fuel(value):
    fuel = float(value)
    if fuel > max_fuel:
        fuel = max_fuel
    if fuel < 0:
        fuel = 0
    PlayerData.set_player_fuel(fuel/max_fuel * 100)
    emit_signal('fuel_changed', fuel/max_fuel * 100)

func get_input(delta) -> void:
    # zero input
    $ThrusterFlame.hide()
    thrust = Vector2(0, 0)
    steering_direction = 0
    # no input possible if not ACTIVE
    if state in [INACTIVE, DISINTEGRATED]:
        return
    if Input.is_action_pressed("thrust") and fuel > delta:
        thrust += Vector2(engine_power, 0)
        self.fuel -= delta
        $ThrusterFlame.show()
    if Input.is_action_pressed("rotate_right") and fuel > delta:
        steering_direction += 1
        self.fuel -= delta /4
    if Input.is_action_pressed("rotate_left") and fuel > delta:
        steering_direction -= 1
        self.fuel -= delta /4

func set_to_start_position(pos, rot, bod) -> void:
    set_position(pos)
    set_rotation(rot)
    astral_bodies = bod
    fuel = 100

func reach_target() -> void:
    change_state(INACTIVE)
    start_body.slots[start_body.last_used_slot] = 2
    PlayerData.player_score += PlayerData.player_fuel
    emit_signal("target_reached")

func disintegrate() -> void:
    call_deferred("set_mode", RigidBody2D.MODE_STATIC)
    $Explosion.show()
    $Explosion/AnimationPlayer.play("explosion")
    yield(get_tree().create_timer(0.22), "timeout")
    change_state(DISINTEGRATED)
    #start_body.slots[start_body.last_used_slot] = 0
    PlayerData.player_disintegrations += 1
    emit_signal("disintegrated")

func _on_ActiveShip_body_entered(body: Node) -> void:
    if state != ACTIVE or body == start_body:
        return
    elif body == target_body:
        reach_target()
        return
    elif body.is_in_group('obstacle'):
        disintegrate()
        return

func _on_AnimationPlayer_animation_finished(anim_name):
    $Explosion.hide()
