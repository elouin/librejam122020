extends Area2D

var timer_constant = 0.1
var flight_record = []
var flight_record_length = 0
var current_record_index = 0
var last_record
var next_record
var state = null
enum {INACTIVE, ACTIVE}

func _ready() -> void:
    $ThrusterFlame.hide()
    change_state(INACTIVE)
        
func _process(delta) -> void:
    if state == ACTIVE:
        var percentage = $FlightRecordTimer.get_time_left()/timer_constant
        var new_pos = percentage * last_record[0] + (1-percentage) * next_record[0]
        # substract 2*PI from last rotation
        var new_rot
        if last_record[1] > 2 and sign(last_record[1]) != sign(next_record[1]):
            new_rot = percentage * (last_record[1] - 2*PI) + (1-percentage) * next_record[1]
        # add 2*PI to last rotation
        elif last_record[1] < -2 and sign(last_record[1]) != sign(next_record[1]):
            new_rot = percentage * (last_record[1] + 2*PI) + (1-percentage) * next_record[1]
        #
        else:
            new_rot = percentage * last_record[1] + (1-percentage) * next_record[1]
        #
        set_pos_rot_thrust([new_pos, new_rot, last_record[2]])
                        
func change_state(new_state) -> void:
    match new_state:
        # collision off
        INACTIVE:
            $CollisionShape2D.set_deferred("disabled", true)
        # collision on
        ACTIVE:
            $CollisionShape2D.set_deferred("disabled", false)
    state = new_state
                        
func start() -> void:
    set_pos_rot_thrust(get_flight_record(0))
    current_record_index = 0
    set_current_record()
    $FlightRecordTimer.set_wait_time(timer_constant)
    $FlightRecordTimer.start()
    change_state(ACTIVE)
        
func _on_FlightRecordTimer_timeout() -> void:
    current_record_index += 1
    set_current_record()
    
func set_current_record():
    if current_record_index >= flight_record_length:
        start()
    last_record = get_flight_record(current_record_index)
    next_record = get_flight_record(current_record_index+1)

func set_pos_rot_thrust(record: Array) -> void:
    set_position(record[0])
    set_rotation(record[1])
    if record[2]:
        $ThrusterFlame.show()
    else:
        $ThrusterFlame.hide()
    
func set_flight_record(new_array: Array) -> void:
    flight_record = [] + new_array
    flight_record_length = len(flight_record)

func get_flight_record(index: int) -> Array:
    if index >= flight_record_length:
        index = flight_record_length - 1
    return flight_record[index]
    
func reset() -> void:
    change_state(INACTIVE)
    $FlightRecordTimer.stop()
    $FlightRecordTimer.set_wait_time(timer_constant)
    current_record_index = 0
    set_pos_rot_thrust(get_flight_record(0))
    $ThrusterFlame.hide()

func _on_OtherShip_body_entered(body):
    if body.is_in_group('player'):
        body.disintegrate()
